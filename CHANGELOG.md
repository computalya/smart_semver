# CHANGE LOG

## v0.5.0 (2021-01-16)

- test with ruby 3.0
- update e-mail address in gemspec
- add minitest
- add tests to minitest
- remove rspec
- update Rakefile for `rake test`
- rename master branch to **main**

## 0.4.0

- update version in object
