require "smart_semver/version"
require "smart_semver/help"
require "smart_semver/semver"

module SmartSemver
  class Error < StandardError; end
end
