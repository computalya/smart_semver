require_relative 'semver/patch'
require_relative 'semver/minor'
require_relative 'semver/major'
require_relative 'semver/dev'
require_relative 'semver/beta'
require_relative 'semver/rc'
require_relative 'semver/release'

class Semver
  include Patch
  include Minor
  include Major
  include Dev
  include Beta
  include Rc
  include Release

  attr_accessor :version

  def initialize(version=nil)
    @version = version ||= '0.1.0'
  end

  def version
    @version
  end

  def version=(version)
    before = @version
    @version = version

    # if invalid versioning sytax, reset value 
    unless valid?
      @version = before
    end
  end

  def increment(current, which)
    version, flag = current.split '-'
    v = version.split '.'
    v[which] = v[which].to_i + 1
    [v.join('.'), flag].compact.join '-'
  end

  def reset(current, which)
    version, flag = current.split '-'
    v = version.split '.'
    which.each do |part|
      v[part] = 0
    end
    [v.join('.'), flag].compact.join '-'
  end

  def valid?
    pattern = /^\d+\.\d+\.\d+(\-(dev|beta|rc\d+))?$/
    version =~ pattern ? true : false
  end
end
