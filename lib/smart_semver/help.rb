module SmartSemver
  HELP = "Semver.new                   # generates default version '0.1.0'\n" \
         "Semver.new '1.2.3'           # genereate Semver object with given version\n" \
         "\n" \
         "x = Semver.new '1.2.3'\n" \
         "x.version                    # => \"1.2.3\"\n" \
         "x.version = \"2.2.2\"\n" \
         "x.valid?                     # => true\n" \
         "\n" \
         "x = Semver.new               # => 0.1.0\n" \
         "x.patch                      # => 0.1.1\n" \
         "x.minor                      # => 0.2.0\n" \
         "x.major                      # => 1.0.0\n" \
         "x.dev                        # => 0.1.0-dev\n" \
         "x.beta                       # => 0.1.0-beta\n" \
         "x.rc                         # => 0.1.0-rc\n" \
         "\n" \
         "x = Semver.new '1.0.0-rc1'\n" \
         "x.release                    # removes all flags\n" \
         "# => \"1.0.0\"\n"

end
