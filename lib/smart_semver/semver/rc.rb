module Rc
  def rc
    ver, flag = version.split '-'
    rc = /^rc(\d+)$/.match flag
    if rc
      self.version = [ver, "rc#{rc[1].to_i+1}"].join '-'
    else
      self.version = [ver, 'rc1'].join '-'
    end
  end
end
