module Minor
  def minor
    incremented = increment(version, 1)

    self.version = reset(incremented, [2]) 
    version
  end
end
