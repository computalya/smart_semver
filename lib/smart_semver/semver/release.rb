module Release
  def release
    # Removes any version flags
    ver, flag = version.strip.split('-').first

    self.version = ver
    version
  end
end
