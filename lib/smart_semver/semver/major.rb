module Major
  def major
    incremented = increment(version, 0)

    self.version = reset incremented, [1, 2] 
    version
  end
end
