module Beta
  def beta
    ver = version.strip.split('-').first            # first remove existing flags
    self.version = [ver, 'beta'].join('-')          # add beta flag
  end
end
