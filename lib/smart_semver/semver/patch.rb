module Patch
  def patch
    self.version = increment version, 2
    version
  end
end
