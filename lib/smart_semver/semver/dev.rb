module Dev
  def dev
    ver = version.strip.split('-').first    # first remove existing flags

    self.version = [ver, 'dev'].join('-')   # add dev flag
  end
end
