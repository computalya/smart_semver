require "bundler/setup"
require "smart_semver"
require 'minitest/autorun'

class TestSmartSemver < MiniTest::Test
  def test_version_is_not_nil
    refute_nil SmartSemver::VERSION
  end

  def test_has_a_help_text
    assert_instance_of(String, SmartSemver::HELP)
  end

  def test_default_value
    assert_equal("0.1.0", Semver.new.version)
  end

  def test_has_correct_version
    x = Semver.new '1.2.3'
    assert_equal("1.2.3", x.version)
  end

  def test_allows_update_of_version
    x = Semver.new '1.2.3'
    x.version = '2.2.2'
    assert_equal("2.2.2", x.version)
  end

  def test_validate_before_update
    x = Semver.new
    x.version='invalid-syntax'
    assert_equal("0.1.0", x.version)
  end

  def test_syntax_is_valid
    x = Semver.new
    assert x.valid?
  end

  def test_syntax_is_invalid
    x = Semver.new 'invalid-versioning'
    refute x.valid?
  end

  def test_patch
    assert_equal("0.1.1", Semver.new.patch)
  end

  def test_patch_dev
    x = Semver.new '0.0.0-dev'
    assert_equal("0.0.1-dev", x.patch)
  end

  def test_patch_object_update
    x = Semver.new
    x.patch
    assert_equal("0.1.1", x.version)
  end

  def test_minor
    assert_equal("0.2.0", Semver.new.minor)
  end

  def test_minor_dev
    x = Semver.new '0.1.0-dev'
    assert_equal("0.2.0-dev", x.minor)
  end

  def test_minor_object_update
    x = Semver.new
    x.minor
    assert_equal("0.2.0", x.version)
  end

  def test_major
    assert_equal("1.0.0", Semver.new.major)
  end

  def test_major_dev
    x = Semver.new '0.1.0-dev'
    assert_equal("1.0.0-dev", x.major)
  end

  def test_major_object_update
    x = Semver.new
    x.major
    assert_equal("1.0.0", x.version)
  end

  def test_dev
    assert_equal("0.1.0-dev", Semver.new.dev)
  end

  def test_dev_object_update
    x = Semver.new
    x.dev
    assert_equal("0.1.0-dev", x.version)
  end

  def test_beta
    assert_equal("0.1.0-beta", Semver.new.beta)
  end

  def test_beta_object_update
    x = Semver.new
    x.beta
    assert_equal("0.1.0-beta", x.version)
  end

  def test_rc
    assert_equal("0.1.0-rc1", Semver.new.rc)
  end

  def test_rc1_rc2
    x = Semver.new('0.1.0-rc1') 
    assert_equal("0.1.0-rc2", x.rc)
  end

  def test_rc_object_update
    x = Semver.new
    x.rc
    assert_equal("0.1.0-rc1", x.version)
  end

  def test_release_rc
    x = Semver.new.rc
    y = Semver.new x
    assert_equal("0.1.0", y.release)
  end

  def test_release_dev
    x = Semver.new '0.1.0-dev'
    assert_equal("0.1.0", x.release)
  end

  def test_release_beta
    x = Semver.new '0.1.0-beta'
    assert_equal("0.1.0", x.release)
  end

  def test_release_object_update
    x = Semver.new '0.1.0-beta'
    x.release
    assert_equal("0.1.0", x.version)
  end
end
