# SmartSemver

Simple **Semantic Versioning** tool for ruby.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'smart_semver'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install smart_semver

## Usage

```ruby
SmartSemver::VERSION
# => "0.3.0"

puts SmartSemver::HELP
# Semver.new                   # generates default version '0.1.0'
# Semver.new '1.2.3'           # genereate Semver object with given version
# 
# x = Semver.new '1.2.3'
# x.version                    # => "1.2.3"
# x.version = "2.2.2"
# x.valid?                     # => true
# 
# x = Semver.new               # => 0.1.0
# x.patch                      # => 0.1.1
# x.minor                      # => 0.2.0
# x.major                      # => 1.0.0
# x.dev                        # => 0.1.0-dev
# x.beta                       # => 0.1.0-beta
# x.rc                         # => 0.1.0-rc

x = Semver.new '1.0.0-rc1'
x.release                    # removes all flags
# => "1.0.0"

Semver.new            # generates default version '0.1.0'
Semver.new '1.2.3'    # genereate Semver object with given version

x = Semver.new '1.2.3'
x.version
# => "1.2.3"

x.version = "2.2.2"

x.valid?
# => true

x = Semver.new
# => "0.1.0"

x.patch     # => 0.1.1
x.minor     # => 0.2.0
x.major     # => 1.0.0
x.dev       # => 0.1.0-dev
x.beta      # => 0.1.0-beta
x.rc        # => 0.1.0-rc

x = Semver.new '1.0.0-rc1'
x.release                      # removes all flags
# => "1.0.0"
```

## Development Notes

### Adding new tags

```bash
git tag -a v0.1.0 -m 'smart_semver-0.1.0'
git push --tags
```

### running tests

```bash
rake test

# or
ruby test/test_smart_semver.rb
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
